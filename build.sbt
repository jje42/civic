import com.gilcloud.sbt.gitlab.GitlabPlugin

Global / onChangedBuildSource := ReloadOnSourceChanges

name := "civic"
organization := "com.gitlab.atgc"
version := "1.0.2"
scalaVersion := "2.12.10"
scalacOptions := Seq("-deprecation")
libraryDependencies ++= Seq(
    "com.fasterxml.jackson.core" % "jackson-databind" % "2.10.2",
    "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.10.2",
    "commons-io" % "commons-io" % "2.6"
)


GitlabPlugin.autoImport.gitlabProjectId := Some(27272577)
credentials += Credentials(Path.userHome / ".sbt" / ".credentials")
