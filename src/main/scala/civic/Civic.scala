/**
  * This is a mapping of the CIViC JSON API format to objects. 
  *
  * The JSON mapper being used automatically maps snake case to camel case
  * (entrez_name in the JSON becomes entrezName in code). It will ignore extra
  * fields in the JSON, but throw an exception if field in the objects are
  * missing in the JSON (unless there are explicitly marked as Option).
  * Anything that can be null in the CIViC data must be marked as an Option
  * here.
  */
package civic

import scala.util.Try
import java.io.File
import java.io.Reader
import java.io.BufferedReader
import java.io.FileInputStream
import java.io.InputStream
import java.io.InputStreamReader
import java.util.zip.GZIPInputStream
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.PropertyNamingStrategy
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import org.apache.commons.io.IOUtils


object Civic {

  private val mapper = new ObjectMapper() with ScalaObjectMapper
  mapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE)
  mapper.registerModule(DefaultScalaModule)
  mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
  mapper.configure(DeserializationFeature.FAIL_ON_NULL_CREATOR_PROPERTIES, true)

  private def fromResource[T](resource: String)(implicit m: Manifest[T]): T = {
    val stream = this.getClass.getClassLoader.getResourceAsStream(resource)
    try
      fromInputStream[T](stream)
    finally
      stream.close()
  }

  private def fromInputStream[T](stream: InputStream)(implicit m: Manifest[T]): T =
    fromString[T](IOUtils.toString(stream, "UTF-8"))

  private def fromString[T](json: String)(implicit m: Manifest[T]): T =
    mapper.readValue[T](json)

  private def reader(f: File): BufferedReader =  {
    val is = 
      if (f.getName.endsWith(".gz")) 
        new GZIPInputStream(new FileInputStream(f)) 
      else 
        new FileInputStream(f)
    new BufferedReader(new InputStreamReader(is, "UTF-8"))
  }

  private def fromReader[T](x: Reader)(implicit m: Manifest[T]): T =
    fromString(IOUtils.toString(x))

  private def _fromFile[T](x: File)(implicit m: Manifest[T]): T =
    fromReader[T](reader(x))

  def toJson(value: Any): String =
    mapper.writeValueAsString(value)

  def load(resource: String = "civic.json"): civic.model.Civic = 
    fromResource[civic.model.Civic](resource)

  def fromFile(f: File): civic.model.Civic = 
    _fromFile[civic.model.Civic](f)
}
