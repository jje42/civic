package civic.model

case class AtgcData(
  hgvsg: Option[String],
  hgvsc: Option[String],
  hgvsp: Option[String],
  vepConsequences: Option[Seq[String]],
  classifications: Seq[String],
  variantType: Option[String]
)

