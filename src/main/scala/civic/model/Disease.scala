package civic.model


case class Disease(
  id: Int,
  name: String,
  displayName: String,
  doid: Option[String],
  url: Option[String],
  reportType: String)
