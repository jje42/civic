package civic.model

case class VariantType(
  id: Int,
  name: String,
  displayName: String,
  soId: String,
  description: String,
  url: String
)

