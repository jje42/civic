package civic.model

case class Drug(
  id: Int,
  name: String,
  pubchemId: Option[String])
