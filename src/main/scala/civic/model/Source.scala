package civic.model

case class Source(
  id: Int,
  name: Option[String],
  citation: String,
  citationId: String,
  sourceType: String,
  ascoAbstractId: Option[String],
  sourceUrl: String,
  openAccess: Option[Boolean],
  pmcId: Option[String],
  publicationDate: PublicationDate,
  journal: Option[String],
  fullJournalTitle: Option[String],
  status: String,
  isReview: Boolean
)

