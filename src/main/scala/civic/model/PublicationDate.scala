package civic.model

case class PublicationDate(
  year: Int,
  month: Option[Int],
  day: Option[Int])
