package civic.model

case class EvidenceItem(
  id: String,
  name: String,
  description: String,
  disease: Disease,
  drugs: Seq[Drug],
  rating: Option[Int],
  evidenceLevel: String,
  evidenceType: String,
  clinicalSignificance: Option[String],
  evidenceDirection: Option[String],
  variantOrigin: Option[String],
  drugInteractionType: Option[String],
  status: String,
  openChangeCount: Int,
  source: Source,
  variantId: Int
)


