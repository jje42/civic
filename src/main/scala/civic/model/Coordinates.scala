package civic.model

case class Coordinates(
  chromosome: Option[String],
  start: Option[Int],
  stop: Option[Int],
  referenceBases: Option[String],
  variantBases: Option[String],
  representativeTranscript: Option[String],
  chromosome2: Option[String],
  start2: Option[Int],
  stop2: Option[Int],
  representativeTranscript2: Option[String],
  ensemblVersion: Option[Int],
  referenceBuild: Option[String]
)

