package civic.model

case class Variant(
  id: Int,
  entrezName: String,
  entrezId: Int,
  name: String,
  description: String,
  geneId: Int,
  variantTypes: Seq[VariantType],
  coordinates: Coordinates,
  evidenceItems: Seq[EvidenceItem],
  hgvsExpression: Option[Seq[String]],
  clinvarEntries: Option[Seq[String]],
  atgc: AtgcData
)
