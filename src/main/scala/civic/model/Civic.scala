package civic.model

case class Civic(
  version: String,
  downloadDate: String,
  variants: Seq[Variant],
  availableReportTypes: Seq[String],
  availableVariantClassifications: Seq[String]
)

